import numpy as np


def spectrum_point(t, io, scr, s1r, tl, kd, fa=1, fv=1, na=1, nv=1):
    scs = scr * fa * na
    s1s = s1r * fv * nv
    ts = s1s / scs
    t0 = ts * 0.2

    scd = scs * kd
    s1d = s1s * kd

    if t < t0:
        sa = scd * (0.4 + (0.6 * (t / t0)))
    elif t < ts:
        sa = scd
    elif t < tl:
        sa = min(s1d / t, scd)
    else:
        sa = (s1d / (t ** 2)) * tl

    return [t, sa]


def spectrum(t_max, io, scr, s1r, tl, kd, t_delta=0.1, t_min=0, fa=1, fv=1, na=1, nv=1):
    return np.array([
        spectrum_point(t, io, scr, s1r, tl, kd, fa=1, fv=1, na=1, nv=1) 
        for t in np.arange(t_min, t_max, t_delta)
    ])


class Spectrum:
    def __init__(self, t_max, io, scr, s1r, tl, kd, t_delta=0.1, t_min=0, fa=1, fv=1, na=1, nv=1):
        self.t_max = t_max
        self.io = io
        self.scr = scr
        self.s1r = s1r
        self.tl = tl
        self.kd = kd
        self.t_delta = t_delta
        self.t_min = t_min
        self.fa = fa
        self.fv = fv
        self.na = na
        self.nv = nv

    @classmethod
    def from_city(cls, city, site_class):
        # todo: get data from AGIES
        return cls(6, 4.1, 1.28, 0.47, 2.48, 0.8, t_delta=0.01)  # Guatemala, Guatemala - site A

    def spectrum(self):
        return spectrum(self.t_max, self.io, self.scr, self.s1r, self.tl, self.kd, self.t_delta, self.t_min, self.fa,
                        self.fv, self.na, self.nv)
